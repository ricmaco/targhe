# -*- coding: utf-8 -*-

project_config = {
    # Files
    'db': 'targhe.sqlite',
    'skel': 'targhe.sql',
    # Application
    'title': 'TARGHE',
    'icon': 'targhe.gif',
    'site': 'http://gringo.linuxvar.it/gitweb/?p=targhe.git'
}