# -*- coding: utf-8 -*-

default_config = {
    # Directories
    'datadir': 'data',
    'resdir': 'res',
    'logdir': 'logs',
    'dbdir': 'database',
    # Files
    'db': 'database.sqlite',
    'skel': 'skeleton.sql',
    # Application
    'title': 'Database',
    'icon': None,
    'site': '',
    'tk': 'http://www.tcl.tk/',
    # i18n
    'lang_nt': 'ita',
    'lang_posix': 'it_IT'
}
