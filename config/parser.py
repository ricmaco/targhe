# -*- coding: utf-8 -*-

import default
import project


class Parser:

    ROOT = None

    def __init__(self, root):
        self.ROOT = root  # lint:ok
        self.config = {}
        self.__build_config()

    def __build_config(self):
        def_conf = default.default_config
        proj_conf = project.project_config
        for key, value in def_conf.items():
            if key in proj_conf:
                self.config[key] = proj_conf[key]
            else:
                self.config[key] = value

    def get(self, key=None):
        if key:
            try:
                return self.config[key]
            except KeyError:
                raise
        return self.config