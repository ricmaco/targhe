# -*- coding: utf-8 -*-

from os import path, makedirs
from datetime import datetime as dt
import calendar as cal


# check if 'folder' folder exists, otherwise create it
def check_dir(folder):
    try:
        makedirs(folder)
    except OSError:
        if not path.isdir(folder):
            raise


# remove multiple tabs and lf/cr from string (for log)
def purify(string):
    temp = ''
    tab = False
    for char in string:
        if char != '\r' and char != '\n':
            if char == '\t':
                if not tab:
                    temp += ' '
                    tab = True
            else:
                temp += char
                tab = False
    return temp


# returns the length of the maximum item of an iterable
# truncated to 25 (it serves the ui)
def max_len(iterable):
    ret = len(max(iterable, key=len))
    if ret > 25:
        return 25
    return ret


# takes iterables like [1, 2, 3, 4, ..., 10, 11, ..., n]
# returns list like ['01', '02', '03', '04', ..., '10', '11', ..., 'n']
# [!] for needed functionality and speed it works for numbers until 99
def trail_zero(iterable):
    ret = []
    for i in iterable:
        temp = str(i)
        if len(temp) == 1:
            ret.append('0' + temp)
        else:
            ret.append(temp)
    return ret


# builds a new list of strings (from iterable) that contain needle
# case-insensitive
def are_in(iterable, needle):
    new_list = []
    needle_lc = needle.lower()
    for haystack in iterable:
        haystack_lc = haystack.lower()
        if needle_lc in haystack_lc:
            new_list.append(haystack)
    return new_list


# return current datetime in a dictionary like:
# {'hour': '00', 'minute': '00', ... }
def curr_dt():
    # get current datetime
    curr_ts = dt.now().strftime('%H-%M-%S-%Y-%m-%d').split('-')
    curr = {}
    # str(int()) to remove trailing zero
    curr['hour'] = curr_ts[0]
    curr['min'] = curr_ts[1]
    curr['sec'] = curr_ts[2]
    curr['year'] = curr_ts[3]
    curr['month'] = curr_ts[4]
    curr['day'] = curr_ts[5]
    return curr


# months names according to locale indexed at zero
# start from index 1, because 0 is empty string
# returns list
def get_months():
    months = []
    for month in cal.month_name[1:]:
        months.append(month.lower())
    return months


# get a list of current month days from 1 to 28/29/30/31
# year: string of year
# month: string name of month
# result: list
# use cal.monthrange second value (number of days in month)
def get_days(year, month):
    iyear = int(year)
    imonth = get_months().index(month) + 1
    return range(1, cal.monthrange(iyear, imonth)[1] + 1)


# from a list, returns a list of 1-tuples from the elements of the list
# lst: list to read values
def list_to_lot(lst):
    temp1 = []
    for el in lst:
        temp2 = []
        #if isinstance(el, basestring):
            #temp3 = unicode(el.decode('utf-8'))
        #else:
            #temp3 = el
        #temp2.append(temp3)
        temp2.append(el)
        temp1.append(temp2)
    return temp1


# from a dictionary, returns the list of tuples from the specified keys
# lst: list from where to gather data
# *args: key of the dictionary to include
def dict_to_lot(lst, *args):
    temp1 = []
    for el in lst:
        temp2 = []
        for i in args:
            #if isinstance(el, basestring):
                #temp3 = unicode(el[i].decode('utf-8'))
            #else:
                #temp3 = el[i]
            #temp2.append(temp3)
            temp2.append(el[i])
        temp1.append(temp2)
    return temp1
