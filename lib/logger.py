# -*- coding: utf-8 -*-

from os import path
from datetime import datetime

import utils


class Logger:

    # conf: instance of class Parser
    def __init__(self, conf):
        conf = conf
        # check if folder is present, otherwise create it
        logdir = path.join(conf.ROOT, conf.get('datadir'), conf.get('logdir'))
        utils.check_dir(logdir)
        # open log file
        logpath = path.join(logdir, self.__get_time('file') + '.log')
        self.logfile = open(logpath, 'w')

    # if use is 'file' returns like '05-08-2015_00-04-32'
    # if use is anything other returns like '[05-08-2014 00:04:32]'
    def __get_time(self, use=None):
        if use == 'file':
            return datetime.now().strftime('%d-%m-%Y_%H-%M-%S')
        else:
            return datetime.now().strftime('[%d-%m-%Y %H:%M:%S]')

    def log(self, message):
        try:
            # [05-08-2014 00:04:32] log message
            self.logfile.write('{0} {1}\n'.format(self.__get_time(), message))
            # flush to update instantly
            self.logfile.flush()
        except IOError:
            raise

    def close(self):
        self.logfile.close()