# -*- coding: utf-8 -*-

import sqlite3


class Controller:

    # control the database with queries
    # logger: instance of class Logger
    # db: instance of model
    def __init__(self, logger, db):
        self.logger = logger
        self.db = db
        self.cursor = db.cursor

    # builds the insert query and then execute it
    # table: string name of table
    # data: dictionary of type {'attr': value}
    # returns id if ok or None
    def insert(self, table, data):
        try:
            attr = data.keys()
            query = 'INSERT INTO {0}('.format(table)
            # attr1,attr2,attr3
            query += ','.join(str(i) for i in attr)
            query += ') VALUES('
            # :attr1,:attr2,:attr3
            query += ','.join(':' + str(i) for i in attr)
            query += ')'
            self.cursor.execute(query, data)
            self.logger.log('Executed insert query "{0}".'.format(query))
            return self.cursor.lastrowid
        except sqlite3.DatabaseError as e:
            self.logger.log('Failed to execute insert query ' +
                '"{0}": {1}.'.format(query, e))
            #raise
            return None

    # builds the select query and execute it
    # fields: tuple of strings name of attributes to display
    # alias: dictionary of 'as' statements of type {'attr': alias}
    # table: string containing the FROM statement
    # cond: string containing the WHERE statement
    # order: string containing the ORDER BY statement
    # returns list of dictionaries of type {'attr': value} or None
    def retrieve(self, fields, table, cond=None, alias=None, order=None):
        try:
            query = 'SELECT '
            # builds the union list of fields and alias
            # fields: (attr1, attr2)
            # alias : {'attr1': 'display1'}
            # ['attr1 AS display1', 'attr2']
            fields_as = []
            if alias:
                for i in fields:
                    if i in alias:
                        fields_as.append('{0} AS {1}'.format(i, alias[i]))
                    else:
                        fields_as.append(i)
                query += ','.join(str(i) for i in fields_as)
            else:
                query += ','.join(str(i) for i in fields)
            query += ' FROM {0}'.format(table)
            # add optional WHERE part
            if cond:
                query += ' WHERE {0}'.format(cond)
            # add optional ORDER BY part
            if order:
                query += ' ORDER BY {0}'.format(order)
            self.cursor.execute(query)
            self.logger.log('Executed select query "{0}".'.format(query))
            # build the list of dictionaries
            select = []
            for row in self.cursor:
                element = {}
                for i in row.keys():
                    element[i] = row[i]
                select.append(element)
            return select
        except sqlite3.DatabaseError as e:
            self.logger.log('Failed to execute select query ' +
                '"{0}": {1}.'.format(query, e))
            #raise
            return None

    # builds the select query and execute it
    # fields: tuple of strings name of attributes to display
    # alias: dictionary of 'as' statements of type {'attr': alias}
    # table: string containing the FROM statement
    # cond: string containing the WHERE statement
    # returns dictionary of type {'attr': value} or None
    def retrieveone(self, fields, table, cond=None, alias=None):
        try:
            query = 'SELECT '
            # builds the union list of fields and alias
            # fields: (attr1, attr2)
            # alias : {'attr1': 'display1'}
            # ['attr1 AS display1', 'attr2']
            fields_as = []
            if alias:
                for i in fields:
                    if i in alias:
                        fields_as.append('{0} AS {1}'.format(i, alias[i]))
                    else:
                        fields_as.append(i)
                query += ','.join(str(i) for i in fields_as)
            else:
                query += ','.join(str(i) for i in fields)
            query += ' FROM {0}'.format(table)
            # add optional WHERE part
            if cond:
                query += ' WHERE {0}'.format(cond)
            self.cursor.execute(query)
            self.logger.log('Executed select query "{0}".'.format(query))
            # build the dictionary
            row = self.cursor.fetchone()
            select = {}
            if row:
                for i in row.keys():
                    select[i] = row[i]
                return select
        except sqlite3.DatabaseError as e:
            self.logger.log('Failed to execute select query ' +
                '"{0}": {1}.'.format(query, e))
            #raise
            return None

    # builds the update query and executes it
    # table: string containing table name
    # fields: dictionary of type {'attr': new_value}
    # cond: string containing WHERE statement
    # returns True if successful or False
    def modify(self, table, fields, cond=None):
        try:
            query = 'UPDATE {0} SET '.format(table)
            # attr1=value1,attr2=value2,attr3=value3
            query += ','.join(str(k) + '=' + str(v) for k, v in fields.items())
            # add optional WHERE part
            if cond:
                query += ' WHERE {0}'.format(cond)
            self.cursor.execute(query)
            self.logger.log('Executed update query "{0}".'.format(query))
            return True
        except sqlite3.DatabaseError as e:
            self.logger.log('Failed to execute update query ' +
                '"{0}": {1}.'.format(query, e))
            #raise
            return False

    # builds the delete query and exectues it
    # table: string containing table name
    # cond: string containing WHERE statement
    # returns True if successful or False
    def delete(self, table, cond=None):
        try:
            query = 'DELETE FROM {0}'.format(table)
            # add optional WHERE part
            if cond:
                query += ' WHERE {0}'.format(cond)
            self.cursor.execute(query)
            self.logger.log('Executed delete query "{0}".'.format(query))
            return True
        except sqlite3.DatabaseError as e:
            self.logger.log('Failed to execute delete query ' +
                '"{0}": {1}.'.format(query, e))
            #raise
            return False
