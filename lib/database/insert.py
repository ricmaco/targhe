# -*- coding: utf-8 -*-


class Insert:

    # controller: instance of class Controller
    def __init__(self, controller):
        self.ctrl = controller

    # returns a list of cities by name
    def cities(self):
        ret = []
        data = self.ctrl.retrieve(('c_nome',), 'Citta')
        for elem in data:
            ret.append(elem['c_nome'])
        return ret

    #def people(self):
        #temp = self.retrieve(('*'), 'Persona')
        #return
