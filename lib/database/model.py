# -*- coding: utf-8 -*-

import sqlite3
import shutil
import pickle
from os import path

from lib import utils


class Model:

    __DBPATH = None

    # conf: instance of class Parser
    # logger: instance of class Logger
    def __init__(self, conf, logger):
        self.conf = conf
        self.logger = logger
        # check if 'database' folder exists, otherwise create it
        self.dbdir = path.join(conf.ROOT, conf.get('datadir'),
            conf.get('dbdir'))
        utils.check_dir(self.dbdir)
        # check if database file already exists
        dbpath = path.join(self.dbdir, conf.get('db'))
        self.__DBPATH = dbpath  # store path for future use
        dbnew = False
        if not path.isfile(dbpath):
            dbnew = True
        # try to open database
        try:
            self.db = sqlite3.connect(dbpath)
            self.db.row_factory = sqlite3.Row
            self.db.text_factory = str
            logger.log('Created or opened database at {0}.'.format(dbpath))
            self.cursor = self.db.cursor()
        except sqlite3.OperationalError:
            logger.log('Failed to open database at {0}.'.format(dbpath))
            raise
        # if db is new
        if dbnew:
            # create skeleton tables
            self.__create_tables(conf.get('skel'))
            logger.log('Created skeleton tables in database.')
            # insert regions, provinces and cities
            self.__insert_places()
            logger.log('Inserted regions, provinces and cities.')

    # create tables reading from file containing skel queries
    def __create_tables(self, skel):
        try:
            # try to fecth queries file and execute them
            skelpath = path.abspath(path.join(self.conf.ROOT,
                self.conf.get('resdir'), skel))
            queries = []
            with open(skelpath, 'r') as f:
                queries = f.read().split(';')
            try:
                # do the actual job
                curr_query = ''
                for query in queries:
                    if query:
                        curr_query = utils.purify(query)
                        self.cursor.execute(query)
                self.db.commit()
            except sqlite3.DatabaseError:
                self.db.rollback()
                self.logger.log('Failed to process creation ' +
                    'query "{0}".'.format(curr_query))
                raise
        except IOError:
            self.logger.log('Failed to create skeleton tables in database.')
            raise

    def __insert_places(self):
        # get pickles paths
        regpath = path.join(self.conf.get('resdir'), 'regioni.pck')
        provpath = path.join(self.conf.get('resdir'), 'province.pck')
        citpath = path.join(self.conf.get('resdir'), 'citta.pck')
        # open files and retrieve data
        try:
            with open(regpath, 'r') as f:
                regs = pickle.load(f)
            with open(provpath, 'r') as f:
                provs = pickle.load(f)
            with open(citpath, 'r') as f:
                cities = pickle.load(f)

            # insert regions
            query = 'INSERT INTO Regione(r_nome) VALUES(?)'
            self.cursor.executemany(query, utils.list_to_lot(regs))
            self.db.commit()
            self.logger.log('Inserted regions in table Regione.')
            # insert provinces
            query = 'INSERT INTO Provincia(pr_nome, regione) VALUES(?, ?)'
            self.cursor.executemany(query, utils.dict_to_lot(provs, 'nome',
                'regione'))
            self.db.commit()
            self.logger.log('Inserted provinces in table Provincia.')
            # insert cities
            query = 'INSERT INTO Citta(c_nome, CAP, provincia) VALUES(?, ?, ?)'
            self.cursor.executemany(query, utils.dict_to_lot(cities, 'nome',
                'cap', 'provincia'))
            self.db.commit()
            self.logger.log('Inserted cities in table Citta.')
        except (IOError, pickle.UnpicklingError, sqlite3.DatabaseError):
            self.logger.log('Failed to insert regions, provinces and cities ' +
                'in database.')
            raise

    # get the cursor of the database
    def cursor(self):
        return self.cursor

    # commit database changes
    def commit(self):
        self.db.commit()
        self.logger.log('Committed changes to the database.')

    # undo database changes
    def rollback(self):
        self.db.rollback()
        self.logger.log('Rolled back changes from the database.')

    # close connection with the database
    def close(self):
        self.db.close()
        self.logger.log('Closed connection to the database.')

    # backup the database to save file
    # savefile: string path where to backup database
    # returns True if successful or False
    def db_backup(self, savefile):
        try:
            # if it is initialized
            if self.__DBPATH:
                # backup database
                shutil.copy2(self.__DBPATH, savefile)
                return True
            return False
        except shutil.Error:
            self.logger.log('Could not backup database ' +
                'to {1}.'.format(savefile))
            return False

    # restore the database from backup file
    # openfile: string path from where to restore backup
    # returns True if successful or False
    def db_restore(self, openfile):
        try:
            # if it is initialized
            if self.__DBPATH:
                # backup database
                shutil.copy2(openfile, self.__DBPATH)
                return True
            return False
        except shutil.Error:
            self.logger.log('Could not backup database ' +
                'to {1}.'.format(openfile))
            return False
