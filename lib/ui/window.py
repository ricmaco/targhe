# -*- coding: utf-8 -*-

import Tkinter as tk
import tkFileDialog as tkfd
import tkMessageBox as tkmb
import sys
import webbrowser
from os import path


class Window(tk.Tk):

    # instantiates main window with main menu
    # conf: instance of class Parser
    # db: instance of class Model
    def __init__(self, conf, db):
        # store parameters for future use
        self.conf = conf
        self.db = db
        # creates main window
        tk.Tk.__init__(self)
        # set geometry to minimum VGA
        #self.geometry('640x480')
        # set unresizable
        #self.resizable(width=False, height=False)
        # set window title
        self.title = conf.get('title')
        self.wm_title(self.title)
        # check if window icon provided exists
        iconpath = path.join(conf.ROOT, conf.get('resdir'), conf.get('icon'))
        # if exists apply it
        if path.isfile(iconpath):
            icon = tk.PhotoImage(file=iconpath)
            self.tk.call('wm', 'iconphoto', self._w, icon)
        # draw main menu
        main_menu = self.__main_menu()
        self.config(menu=main_menu)
        # draw context menu
        self.__context_menu()

    def __main_menu(self):
        # draw main menu bar
        main_menu = tk.Menu(self, tearoff=0)

        # draw file dropdown menu
        file_menu = tk.Menu(main_menu, tearoff=0)  # tearoff=0 no detach
        main_menu.add_cascade(label='File', menu=file_menu, underline=0)
        file_menu.add_command(label='Importa database', command=self.db_import,
            accelerator='Ctrl+I')
        self.bind_all('<Control-i>', self.db_import)  # global command
        file_menu.add_command(label='Esporta database', command=self.db_export,
            accelerator='Ctrl+B')
        self.bind_all('<Control-b>', self.db_export)
        file_menu.add_separator()
        file_menu.add_command(label='Esporta PDF', command=self.__pass,
            accelerator='Ctrl+E')
        self.bind_all('<Control-e>', self.__pass)
        file_menu.add_separator()
        file_menu.add_command(label='Esci', command=self.quit,
            accelerator='Ctrl+Q')
        self.bind_all('<Control-q>', self.quit)

        # draw edit dropdown menu
        edit_menu = tk.Menu(main_menu, tearoff=0)
        main_menu.add_cascade(label='Modifica', menu=edit_menu, underline=0)
        edit_menu.add_command(label='Taglia', command=self.cut,
            accelerator='Ctrl+X')
        edit_menu.add_command(label='Copia', command=self.copy,
            accelerator='Ctrl+C')
        edit_menu.add_command(label='Incolla', command=self.paste,
            accelerator='Ctrl+V')

        # draw about dropdown menu
        about_menu = tk.Menu(main_menu, tearoff=0)
        main_menu.add_cascade(label='?', menu=about_menu, underline=0)
        about_menu.add_command(label='Homepage',
            command=lambda: webbrowser.open(self.conf.get('site'), new=2))
        about_menu.add_separator()
        about_menu.add_command(label='Informazioni su TARGHE',
            command=self.__pass)
        about_menu.add_command(label='Informazioni su Tk',
            command=lambda: webbrowser.open(self.conf.get('tk'), new=2))

        return main_menu

    # simple function to debug if event it triggered
    def __pass(self, event=None):
        print('Something happened')

    # draw a contextual menu on right click
    def __context_menu(self):
        cont_menu = tk.Menu(self, tearoff=0)
        cont_menu.add_command(label='Taglia', command=self.cut,
            accelerator='Ctrl+X')
        cont_menu.add_command(label='Copia', command=self.copy,
            accelerator='Ctrl+C')
        cont_menu.add_command(label='Incolla', command=self.paste,
            accelerator='Ctrl+V')
        # bind menu to right click
        self.bind('<Button-3>',
            lambda(e): cont_menu.post(e.x_root, e.y_root))

    # generate default system cut event
    def cut(self, widget=None):
        if widget:
            widget.focus_get().event_generate('<<Cut>>')
        else:
            self.focus_get().event_generate('<<Cut>>')

    # generate default system copy event
    def copy(self, widget=None):
        if widget:
            widget.focus_get().event_generate('<<Copy>>')
        else:
            self.focus_get().event_generate('<<Copy>>')

    # generate default system paste event
    def paste(self, widget=None):
        if not widget:
            widget = self
        widget.focus_get().event_generate('<<Paste>>')

    def db_export(self, event=None):
        # set tkFileDialog options
        options = {
            'defaultextension': '.sqlite',
            'filetypes': [('tutti i file', '.*'),
                ('database file', '.sqlite')],
            'initialfile': 'backup.sqlite',
            'parent': self,
            'title': 'Scegli dove salvare il file di backup'
        }
        # retrieve path where to backup database
        savefile = tkfd.asksaveasfilename(**options)
        # if exporting goes well
        if self.db.db_backup(savefile):
            # show a window
            tkmb.showinfo(self.title, 'Database esportato con ' +
                'successo in {0}.'.format(savefile))
        else:
            tkmb.showerror(self.title, 'Impossibile esportare ' +
                'database.')

    def db_import(self, event=None):
        # set tkFileDialog options
        options = {
            'defaultextension': '.sqlite',
            'filetypes': [('tutti i file', '.*'),
                ('database file', '.sqlite')],
            'initialfile': 'backup.sqlite',
            'parent': self,
            'title': 'Scegli dove salvare il file di backup'
        }
        # retrieve path from where to restore database
        openfile = tkfd.askopenfilename(**options)
        # if importing goes well
        if self.db.db_restore(openfile):
            # show a window
            tkmb.showinfo(self.title, 'Database importato con ' +
                'successo in {0}.'.format(openfile))
            self.quit()
        else:
            tkmb.showerror(self.title, 'Impossibile importare ' +
                'database.')

    # simple quit function
    def quit(self, event=None):
        sys.exit(0)