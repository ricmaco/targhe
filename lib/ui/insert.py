# -*- coding: utf-8 -*-

import Tkinter as tk
import ttk

from lib import utils


# builds insert tab widgets
class Insert:

    def __init__(self, content, frame, db_insert):
        self.frame = frame
        self.content = content
        self.db_ins = db_insert
        self.__create_widgets()
        self.__update()

    # updates view with a button
    # only for testing purposes
    def __update(self):
        button_frame = ttk.Frame(self.frame)
        button_frame.pack(side=tk.BOTTOM, padx=5, pady=10)
        btn = ttk.Button(button_frame, text='Update',
            command=self.__update_action)
        btn.pack(side=tk.RIGHT, padx=25)

    # define action of the update button
    def __update_action(self):
        self.top_frame.destroy()
        self.middle_frame.destroy()
        self.bottom_frame.destroy()
        self.end_frame.destroy()
        self.__create_widgets()

    # draw widgets in the current frame
    def __create_widgets(self):
        # fetch current datetime
        curr = utils.curr_dt()
        self.curr_hour = curr['hour']
        self.curr_min = curr['min']
        self.curr_sec = curr['sec']
        self.curr_year = curr['year']
        self.curr_month = curr['month']
        self.curr_day = curr['day']
        # get list of months according to locale
        self.months = utils.get_months()
        # get a list of cities
        self.cities = self.db_ins.cities()
        # define four frames to divide forms
        self.top_frame = ttk.Frame(self.frame)
        self.top_frame.pack(padx=5, pady=10)
        self.middle_frame = ttk.Frame(self.frame)
        self.middle_frame.pack(padx=5, pady=10)
        self.bottom_frame = ttk.Frame(self.frame)
        self.bottom_frame.pack(padx=5, pady=10)
        self.end_frame = ttk.Frame(self.frame)
        self.end_frame.pack(padx=5, pady=10)
        # date
        self.__date()
        # place
        self.__place()
        # person
        self.__person()
        # vehicle
        self.__vehicle()
        # towing
        self.__towing()
        # stop
        self.__stop()

    # draws date labelframe
    # returns date labelframe
    def __date(self):
        date = ttk.LabelFrame(self.top_frame, text='Data')
        date.pack(side=tk.LEFT, padx=30)
        # time label
        time_label = tk.Label(date, text='Ora:')
        time_label.grid(column=0, row=0, sticky=tk.W)
        # hour
        self.__date_hour(date)
        # minute
        self.__date_min(date)
        # second
        self.__date_sec(date)
        # date label
        date_label = tk.Label(date, text='Data:')
        date_label.grid(column=0, row=1, sticky=tk.W)
        # year
        self.__date_year(date)
        # month
        month = self.__date_month(date)
        month.bind('<<ComboboxSelected>>', lambda(e): self.__date_day(date))
        # day
        self.__date_day(date)
        return date

    # draws hour combobox in date labelframe
    # returns hour combobox
    def __date_hour(self, date):
        self.d_hourvar = tk.StringVar()
        hours = utils.trail_zero(range(0, 24))
        hour = ttk.Combobox(date, textvariable=self.d_hourvar,
            values=hours, width=2, state='readonly')
        hour.grid(column=1, row=0, sticky=tk.W, padx=20, pady=10)
        hour.set(self.curr_hour)
        return hour

    # draws minutes combobox in date labelframe
    # returns minutes combobox
    def __date_min(self, date):
        self.d_minvar = tk.StringVar()
        mins = utils.trail_zero(range(0, 60))
        minute = ttk.Combobox(date, textvariable=self.d_minvar,
            values=mins, width=2, state='readonly')
        minute.grid(column=2, row=0, sticky=tk.W, padx=5, pady=10)
        minute.set(self.curr_min)
        return minute

    # draws seconds combobox in date labelframe
    # returns seconds combobox
    def __date_sec(self, date):
        self.d_secvar = tk.StringVar()
        secs = utils.trail_zero(range(0, 60))
        second = ttk.Combobox(date, textvariable=self.d_secvar,
            values=secs, width=2, state='readonly')
        second.grid(column=3, row=0, sticky=tk.W, padx=5, pady=10)
        second.set(self.curr_sec)
        return second

    # draws year combobox in date labelframe
    # returns year combobox
    def __date_year(self, date):
        self.d_yearvar = tk.StringVar()
        # from 2015 (date of code creation) until curr year
        years = range(2015, int(self.curr_year) + 1)
        year = ttk.Combobox(date, textvariable=self.d_yearvar,
            values=years, width=4, state='readonly')
        year.grid(column=1, row=1, sticky=tk.W, padx=20, pady=10)
        year.set(self.curr_year)
        return year

    # draws month combobox in date labelframe
    # returns month combobox
    def __date_month(self, date):
        self.d_monthvar = tk.StringVar()
        month = ttk.Combobox(date, textvariable=self.d_monthvar,
            values=self.months, width=9, state='readonly')
        month.grid(column=2, row=1, sticky=tk.W, padx=5, pady=10)
        month.set(self.months[int(self.curr_month) - 1])
        return month

    # draws day combobox in date labelframe
    # returns day combobox
    def __date_day(self, date):
        self.d_dayvar = tk.StringVar()
        temp_days = utils.get_days(self.d_yearvar.get(),
            self.d_monthvar.get())
        days = utils.trail_zero(temp_days)
        day = ttk.Combobox(date, textvariable=self.d_dayvar,
            values=days, width=2, state='readonly')
        day.grid(column=3, row=1, sticky=tk.W, padx=5, pady=10)
        day.set(self.curr_day)
        return day

    # draws place labelframe
    # returns place labelframe
    def __place(self):
        place = ttk.LabelFrame(self.top_frame, text='Luogo')
        place.pack(side=tk.LEFT, padx=30)
        # city label
        city_label = tk.Label(place, text='Località:')
        city_label.grid(column=4, row=0, sticky=tk.W)
        # city
        self.__place_city(place)
        return place

    # draws city combobox in place labelframe
    # returns city combobox
    def __place_city(self, place):
        self.pl_cityvar = tk.StringVar()
        city = AutocompleteCombobox(place, textvariable=self.pl_cityvar,
            width=utils.max_len(self.cities))
        city.set_completion_list(self.cities)
        city.grid(column=5, row=0, sticky=tk.W, padx=10, pady=10)
        city.set(self.cities[0])
        return city

    # draws person labelframe
    # returns person labelframe
    def __person(self):
        person = ttk.LabelFrame(self.middle_frame, text='Persona')
        person.pack(side=tk.LEFT)
        # name label
        name_label = tk.Label(person, text='Nome:')
        name_label.grid(column=0, row=0, sticky=tk.W)
        # name
        self.__pers_name(person)
        # surname label
        surname_label = tk.Label(person, text='Cognome:')
        surname_label.grid(column=0, row=1, sticky=tk.W)
        # surname
        self.__pers_surname(person)
        # birth city label
        birth_city_label = tk.Label(person, text='Nato a:')
        birth_city_label.grid(column=0, row=2, sticky=tk.W)
        # birth city
        self.__pers_b_city(person)
        # birth date label
        birth_date_label = tk.Label(person, text='il:')
        birth_date_label.grid(column=2, row=2, sticky=tk.W)
        # birth year
        self.__pers_year(person)
        # birth month
        month = self.__pers_month(person)
        month.bind('<<ComboboxSelected>>',
            lambda(e): self.__pers_day(person))
        # birth day
        self.__pers_day(person)
        # city label
        city_label = tk.Label(person, text='Residente a:')
        city_label.grid(column=0, row=3, sticky=tk.W)
        # city
        self.__pers_city(person)
        # address label
        addr_label = tk.Label(person, text='in:')
        addr_label.grid(column=2, row=3, sticky=tk.W)
        # address type
        self.__pers_addr_type(person)
        # address street
        self.__pers_addr_street(person)
        # address number
        self.__pers_addr_no(person)
        return person

    # draws name entry in person labelframe
    # returns name entry
    def __pers_name(self, person):
        self.p_namevar = tk.StringVar()
        name = ttk.Entry(person, textvariable=self.p_namevar, width=25)
        name.grid(column=1, row=0, columnspan=2, sticky=tk.W,
            padx=10, pady=10)
        return name

    # draws surname entry in person labelframe
    # returns surname entry
    def __pers_surname(self, person):
        self.p_surnamevar = tk.StringVar()
        surname = ttk.Entry(person, textvariable=self.p_surnamevar, width=25)
        surname.grid(column=1, row=1, columnspan=2, sticky=tk.W,
            padx=10, pady=10)
        return surname

    # draws birth city combobox in person labelframe
    # returns birth city combobox
    def __pers_b_city(self, person):
        self.p_b_cityvar = tk.StringVar()
        city = AutocompleteCombobox(person, textvariable=self.p_b_cityvar,
            width=utils.max_len(self.cities))
        city.set_completion_list(self.cities)
        city.grid(column=1, row=2, sticky=tk.W, padx=10, pady=10)
        city.set(self.cities[0])
        return city

    # draws year combobox in person labelframe
    # returns year combobox
    def __pers_year(self, date, event=None):
        self.p_yearvar = tk.StringVar()
        # from 2015 (date of code creation) until curr year
        years = range(1900, int(self.curr_year) + 1)
        year = ttk.Combobox(date, textvariable=self.p_yearvar,
            values=years, width=4, state='readonly')
        year.grid(column=3, row=2, sticky=tk.W, padx=10, pady=10)
        year.set(1960)
        return year

    # draws month combobox in person labelframe
    # returns month combobox
    def __pers_month(self, date):
        self.p_monthvar = tk.StringVar()
        month = ttk.Combobox(date, textvariable=self.p_monthvar,
            values=self.months, width=9, state='readonly')
        month.grid(column=4, row=2, sticky=tk.W, padx=5, pady=10)
        month.set(self.months[0])
        return month

    # draws day combobox in person labelframe
    # returns day combobox
    def __pers_day(self, date):
        self.p_dayvar = tk.StringVar()
        temp_days = utils.get_days(self.p_yearvar.get(),
            self.p_monthvar.get())
        days = utils.trail_zero(temp_days)
        day = ttk.Combobox(date, textvariable=self.p_dayvar,
            values=days, width=2, state='readonly')
        day.grid(column=5, row=2, sticky=tk.W, padx=5, pady=10)
        day.set('01')
        return day

    # draws city combobox in person labelframe
    # returns city combobox
    def __pers_city(self, person):
        self.p_cityvar = tk.StringVar()
        city = AutocompleteCombobox(person, textvariable=self.p_cityvar,
            width=utils.max_len(self.cities))
        city.set_completion_list(self.cities)
        city.grid(column=1, row=3, sticky=tk.W, padx=10, pady=10)
        city.set(self.cities[0])
        return city

    # draws address type combobox in person labelframe
    # returns address type combobox
    def __pers_addr_type(self, person):
        self.p_addr_typevar = tk.StringVar()
        atypes = (
            'Via',
            'Piazza',
            'Piazzale',
            'Contrada',
            'Calle',
            'Rione'
        )
        atype = ttk.Combobox(person, textvariable=self.p_addr_typevar,
            values=atypes, width=8, state='readonly')
        atype.grid(column=3, row=3, sticky=tk.W, padx=10, pady=10)
        atype.set(atypes[0])
        return atype

    # draws address street entry in person labelframe
    # returns address street entry
    def __pers_addr_street(self, person):
        self.p_addr_streetvar = tk.StringVar()
        street = ttk.Entry(person, textvariable=self.p_addr_streetvar, width=25)
        street.grid(column=4, row=3, sticky=tk.W, padx=5, pady=10)
        return street

    # draws address number entry in person labelframe
    # returns address number entry
    def __pers_addr_no(self, person):
        self.p_addr_novar = tk.StringVar()
        no = ttk.Entry(person, textvariable=self.p_addr_novar, width=3)
        no.grid(column=5, row=3, sticky=tk.W, padx=5, pady=10)
        return no

    # draws vehicle labelframe
    # returns vehicle labelframe
    def __vehicle(self):
        vehicle = ttk.LabelFrame(self.bottom_frame, text='Veicolo')
        vehicle.pack(side=tk.LEFT, padx=30)
        # plate label
        plate_label = tk.Label(vehicle, text='Targa:')
        plate_label.grid(column=0, row=0, sticky=tk.W)
        # plate
        self.__veh_plate(vehicle)
        # brand label
        brand_label = tk.Label(vehicle, text='Marca:')
        brand_label.grid(column=0, row=1, sticky=tk.W)
        # brand
        self.__veh_brand(vehicle)
        # model label
        model_label = tk.Label(vehicle, text='Modello:')
        model_label.grid(column=0, row=2, sticky=tk.W)
        # model
        self.__veh_model(vehicle)
        # color label
        color_label = tk.Label(vehicle, text='Colore:')
        color_label.grid(column=0, row=3, sticky=tk.W)
        # color
        self.__veh_color(vehicle)
        return vehicle

    # draws plate entry in vehicle labelframe
    # returns plate entry
    def __veh_plate(self, vehicle):
        self.v_platevar = tk.StringVar()
        plate = ttk.Entry(vehicle, textvariable=self.v_platevar, width=15)
        plate.grid(column=1, row=0, sticky=tk.W, padx=10, pady=10)
        return plate

    # draws brand entry in vehicle labelframe
    # returns brand entry
    def __veh_brand(self, vehicle):
        self.v_brandvar = tk.StringVar()
        brand = ttk.Entry(vehicle, textvariable=self.v_brandvar, width=15)
        brand.grid(column=1, row=1, sticky=tk.W, padx=10, pady=10)
        return brand

    # draws model entry in vehicle labelframe
    # returns model entry
    def __veh_model(self, vehicle):
        self.v_modelvar = tk.StringVar()
        model = ttk.Entry(vehicle, textvariable=self.v_modelvar, width=15)
        model.grid(column=1, row=2, sticky=tk.W, padx=10, pady=10)
        return model

    # draws color combobox in vehicle labelframe
    # returns color combobox
    def __veh_color(self, vehicle):
        self.v_colorvar = tk.StringVar()
        colors = (
            'grigio',
            'bianco',
            'nero',
            'blu',
            'verde',
            'rosso',
            'rosa',
            'viola',
            'arancione',
            'marrone',
            'giallo',
            'azzurro'
        )
        color = ttk.Combobox(vehicle, textvariable=self.v_colorvar,
            values=colors, width=9)
        color.grid(column=1, row=3, sticky=tk.W, padx=10, pady=10)
        return color

    # draws towing labelframe
    # returns towing labelframe
    def __towing(self):
        towing = ttk.LabelFrame(self.bottom_frame, text='Traino')
        towing.pack(side=tk.LEFT, padx=10)
        # towing label
        towing_label = tk.Label(towing, text='Traino:')
        towing_label.grid(column=0, row=0, sticky=tk.W)
        # plate
        self.__tow_check(towing)
        # towing type label
        tow_type_label = tk.Label(towing, text='Tipo:')
        tow_type_label.grid(column=0, row=1, sticky=tk.W)
        # towing type
        self.__tow_type(towing)
        # towing plate label
        tow_pl_label = tk.Label(towing, text='Targa:')
        tow_pl_label.grid(column=0, row=2, sticky=tk.W)
        # towing plate
        self.__tow_plate(towing)
        return towing

    # draws towing check in towing labelframe
    # returns towing check
    def __tow_check(self, towing):
        self.t_checkvar = tk.StringVar()
        check = ttk.Checkbutton(towing, textvariable=self.t_checkvar,
            onvalue='Sì', offvalue='No',
            command=lambda: self.__tow_checkbutton_toggle(towing))
        check.grid(column=1, row=0, sticky=tk.W, padx=10, pady=10)
        self.t_checkvar.set('No')
        return check

    # lazy ttk developers: toggle not implemented
    # also enables/disables type and plate fields
    def __tow_checkbutton_toggle(self, towing):
        state = self.t_checkvar.get()
        if state == 'No':
            self.t_checkvar.set('Sì')
            self.__tow_type(towing, state='readonly')
            self.__tow_plate(towing, state='normal')
        else:
            self.t_checkvar.set('No')
            self.__tow_type(towing)
            self.__tow_plate(towing)

    # draws type combobox in towing labelframe
    # returns type combobox
    def __tow_type(self, towing, state='disabled'):
        self.t_typevar = tk.StringVar()
        types = (
            'Appendice',
            'Rimorchio',
            'T.A.T.S.'
        )
        ttype = ttk.Combobox(towing, textvariable=self.t_typevar,
            values=types, width=9, state=state)
        ttype.grid(column=1, row=1, sticky=tk.W, padx=10, pady=10)
        return ttype

    # draws plate entry in towing labelframe
    # returns plate entry
    def __tow_plate(self, towing, state='disabled'):
        self.t_platevar = tk.StringVar()
        plate = ttk.Entry(towing, textvariable=self.t_platevar,
            width=15, state=state)
        plate.grid(column=1, row=2, sticky=tk.W, padx=10, pady=10)
        return plate

    # draws stop labelframe
    # returns stop labelframe
    def __stop(self):
        stop = ttk.LabelFrame(self.end_frame, text='Controllo')
        stop.pack(side=tk.LEFT, padx=10)
        # law label
        law_label = tk.Label(stop, text='Norma C.d.S.:')
        law_label.grid(column=0, row=0, sticky=tk.W)
        # law
        self.__stop_law(stop)
        # fee label
        fee_label = tk.Label(stop, text='Contravvenzione:')
        fee_label.grid(column=0, row=1, sticky=tk.W)
        # stop type
        self.__stop_fee(stop)
        return stop

    # draws law entry in stop labelframe
    # returns law entry
    def __stop_law(self, stop):
        self.s_lawvar = tk.StringVar()
        law = ttk.Entry(stop, textvariable=self.s_lawvar,
            width=25)
        law.grid(column=1, row=0, sticky=tk.W, padx=10, pady=10)
        return law

    # draws fee check in stop labelframe
    # returns fee check
    def __stop_fee(self, stop):
        self.s_feevar = tk.StringVar()
        fee = ttk.Checkbutton(stop, textvariable=self.s_feevar,
            onvalue='Sì', offvalue='No',
            command=lambda: self.__stop_checkbutton_toggle(stop))
        fee.grid(column=1, row=1, sticky=tk.W, padx=10, pady=10)
        self.s_feevar.set('No')
        return fee

    # lazy ttk developers: toggle not implemented
    def __stop_checkbutton_toggle(self, stop):
        state = self.s_feevar.get()
        if state == 'No':
            self.s_feevar.set('Sì')
        else:
            self.s_feevar.set('No')


# code from http://stackoverflow.com/questions/12298159/
# tkinter-how-to-create-a-combo-box-with-autocompletion


# autcomplete combobox with a defined list
class AutocompleteCombobox(ttk.Combobox):

    def set_completion_list(self, completion_list):
        """Use our completion list as our drop down selection menu,
            arrows move through menu."""
        # Work with a sorted list
        self._completion_list = sorted(completion_list, key=str.lower)
        self._hits = []
        self._hit_index = 0
        self.position = 0
        self.bind('<KeyRelease>', self.handle_keyrelease)
        self['values'] = self._completion_list  # Setup our popup menu

    def autocomplete(self, delta=0):
        """autocomplete the Combobox, delta may be 0/1/-1
            to cycle through possible hits"""
        # need to delete selection otherwise we would fix the current position
        if delta:
            self.delete(self.position, tk.END)
        # set position to end so selection starts where textentry ended
        else:
            self.position = len(self.get())
        # collect hits
        _hits = []
        for element in self._completion_list:
            # Match case insensitively
            if element.lower().startswith(self.get().lower()):
                _hits.append(element)
        # if we have a new hit list, keep this in mind
        if _hits != self._hits:
            self._hit_index = 0
            self._hits = _hits
        # only allow cycling if we are in a known hit list
        if _hits == self._hits and self._hits:
            self._hit_index = (self._hit_index + delta) % len(self._hits)
        # now finally perform the auto completion
        if self._hits:
            self.delete(0, tk.END)
            self.insert(0, self._hits[self._hit_index])
            self.select_range(self.position, tk.END)

    def handle_keyrelease(self, event):
        """event handler for the keyrelease event on this widget"""
        if event.keysym == "BackSpace":
            self.delete(self.index(tk.INSERT), tk.END)
            self.position = self.index(tk.END)
        if event.keysym == "Left":
            if self.position < self.index(tk.END):  # delete the selection
                self.delete(self.position, tk.END)
            else:
                self.position = self.position - 1  # delete one character
                self.delete(self.position, tk.END)
        if event.keysym == "Right":
            self.position = self.index(tk.END)  # go to end (no selection)
        if len(event.keysym) == 1:
            self.autocomplete()
        # No need for up/down, we'll jump to the popup
        # list at the position of the autocompletion


# autcomplete entry with a defined list
class AutocompleteEntry(tk.Entry):

    def set_completion_list(self, completion_list):
            # Work with a sorted list
            self._completion_list = sorted(completion_list, key=str.lower)
            self._hits = []
            self._hit_index = 0
            self.position = 0
            self.bind('<KeyRelease>', self.handle_keyrelease)

    def autocomplete(self, delta=0):
        """autocomplete the Entry, delta may be 0/1/-1
            to cycle through possible hits"""
        # need to delete selection otherwise we would fix the current position
        if delta:
            self.delete(self.position, tk.END)
        # set position to end so selection starts where textentry ended
        else:
            self.position = len(self.get())
        # collect hits
        _hits = []
        for element in self._completion_list:
            # Match case-insensitively
            if element.lower().startswith(self.get().lower()):
                _hits.append(element)
        # if we have a new hit list, keep this in mind
        if _hits != self._hits:
            self._hit_index = 0
            self._hits = _hits
        # only allow cycling if we are in a known hit list
        if _hits == self._hits and self._hits:
            self._hit_index = (self._hit_index + delta) % len(self._hits)
        # now finally perform the auto completion
        if self._hits:
            self.delete(0, tk.END)
            self.insert(0, self._hits[self._hit_index])
            self.select_range(self.position, tk.END)

        def handle_keyrelease(self, event):
            """event handler for the keyrelease event on this widget"""
            if event.keysym == "BackSpace":
                self.delete(self.index(tk.INSERT), tk.END)
                self.position = self.index(tk.END)
            if event.keysym == "Left":
                # delete the selection
                if self.position < self.index(tk.END):
                    self.delete(self.position, tk.END)
                else:
                    # delete one character
                    self.position = self.position - 1
                    self.delete(self.position, tk.END)
            if event.keysym == "Right":
                # go to end (no selection)
                self.position = self.index(tk.END)
            if event.keysym == "Down":
                # cycle to next hit
                self.autocomplete(1)
            if event.keysym == "Up":
                # cycle to previous hit
                self.autocomplete(-1)
            if len(event.keysym) == 1:
                self.autocomplete()
