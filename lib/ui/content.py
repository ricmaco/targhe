# -*- coding: utf-8 -*-

import Tkinter as tk
import ttk


class Content:

    # define interior widgets of window
    # logger: instance of class Logger
    # window: instance of class window
    def __init__(self, window):
        self.window = window
        self.__create_tabs()

    def __create_tabs(self):
        # Create tabs widget with three tabs
        self.tabs = ttk.Notebook(self.window)
        self.insert = ttk.Frame(self.tabs)  # tab 0
        self.search = ttk.Frame(self.tabs)  # tab 1
        self.edit = ttk.Frame(self.tabs)  # tab 2
        self.tabs.add(self.insert,
            text='          {0}          '.format('Inserisci'))
        self.tabs.add(self.search,
            text='          {0}          '.format('Ricerca'))
        # enabled only by a trigger in search tab
        self.tabs.add(self.edit,
            text='          {0}          '.format('Modifica'),
            state='disabled')
        self.tabs.pack(fill=tk.X)

    # return insert frame
    def insert_tab(self):
        return self.insert

    # return search frame
    def search_tab(self):
        return self.search

    # return edit frame
    def edit_tab(self):
        return self.edit

    # enable/disable edit tab
    def edit_toggle(self, event=None):
        state = self.tabs.tab(2, option='state')
        print state
        if state == 'normal':
            self.tabs.tab(2, state='disabled')
        else:
            self.tabs.tab(2, state='normal')

    # select insert, search or edit tab
    def select(self, tab, event=None):
        if tab == 2:
            state = self.tabs.tab(2, option='state')
            if state == 'normal':
                self.tabs.select(2)
        else:
            self.tabs.select(tab)