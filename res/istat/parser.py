# -*- coding: utf-8 -*-

import sys
import pickle
from os import path


class Parser:

    # reg_prov: string filename of regions, provinces names
    # cities: string filename of cities names
    # zips: string filename of cities zips
    def __init__(self, reg_prov, cities, zips):
        self.reg_prov_file = path.abspath(reg_prov)
        self.cities_file = path.abspath(cities)
        self.zips_file = path.abspath(zips)
        self.regions = {}
        self.provinces = []
        self.cities = []
        self.__parse_zips()
        self.__moar_zips()
        self.__parse_regions()
        self.__parse_provinces()
        self.__parse_cities()
        #print('ZIPS')
        #print(self.zips)

    # regions parsed as:
    # {id: region}
    def __parse_regions(self):
        try:
            with open(self.reg_prov_file, 'r') as f:
                for line in f:
                    chunks = line.split(';')
                    reg_id = int(chunks[5])
                    reg_nome = chunks[9].split('/')[0]
                    if reg_id not in self.regions:
                        self.regions[reg_id] = reg_nome
        except IOError:
            sys.stderr.write('Regioni: errore nell\'apertura del ' +
                'file {0}.\n'.format(self.reg_prov_file))
            sys.stderr.flush()
            raise

    # provinces parsed as:
    # [{'id': 01, 'regione': 01, 'nome': 'Varese'}, ...]
    def __parse_provinces(self):
        try:
            with open(self.reg_prov_file, 'r') as f:
                for line in f:
                    prov = {}
                    chunks = line.split(';')
                    reg_id = int(chunks[5])
                    prov_id = int(chunks[10])
                    prov_nome = chunks[14].split('/')[0]
                    if prov_nome == '-':
                        prov_nome = chunks[15].split('/')[0]
                    prov['id'] = prov_id
                    prov['regione'] = reg_id
                    prov['nome'] = prov_nome
                    self.provinces.append(prov)
        except IOError:
            sys.stderr.write('Province: errore nell\'apertura del ' +
                'file {0}.\n'.format(self.reg_prov_file))
            sys.stderr.flush()

    # zips parsed as:
    # {id: zip}
    def __parse_zips(self):
        try:
            self.zips = {}
            with open(self.zips_file, 'r') as zp:
                for line in zp:
                    chunks = line.split(';')
                    zip_id = int(chunks[0])
                    zip_cap = chunks[5]
                    self.zips[zip_id] = zip_cap
        except IOError:
            sys.stderr.write('Città: errore nell\'apertura del ' +
                'file {0}.\n'.format(self.zips_file))
            sys.stderr.flush()

    # add missing zips to self.zips
    def __moar_zips(self):
        self.zips[12142] = 21061
        self.zips[13250] = 22021
        self.zips[13251] = 22041
        self.zips[13252] = 22016
        self.zips[16252] = 24038
        self.zips[16253] = 24012
        self.zips[18191] = 27056
        self.zips[20071] = 46034
        self.zips[22230] = 38012
        self.zips[22231] = 38078
        self.zips[22232] = 38080
        self.zips[25070] = 32038
        self.zips[25071] = 32013
        self.zips[30188] = 33061
        self.zips[34049] = 43018
        self.zips[37061] = 40053
        self.zips[38027] = 44027
        self.zips[41068] = 61022
        self.zips[42050] = 60012
        self.zips[46036] = 55021
        self.zips[46037] = 55030
        self.zips[48052] = 50063
        self.zips[48053] = 50038
        self.zips[50040] = 56035
        self.zips[50041] = 56042
        self.zips[51040] = 52026
        self.zips[51041] = 52015
        self.zips[64121] = 83025
        self.zips[93053] = 33098
        self.zips[97091] = 23879
        self.zips[97092] = 23888
        self.zips[99028] = 47824

    # cities parsed as:
    # [{'provincia': 01, 'cap': 10101, 'nome': 'Somma Lombardo'}, ...]
    def __parse_cities(self):
        try:
            with open(self.cities_file, 'r') as cit:
                for line in cit:
                    city = {}
                    chunks = line.split(';')
                    prov_id = int(chunks[4])
                    city_id = int(chunks[7])
                    city_nome = chunks[12]
                    city_cap = self.zips[city_id]
                    city['provincia'] = prov_id
                    city['nome'] = city_nome
                    city['cap'] = city_cap
                    self.cities.append(city)
        except IOError:
            sys.stderr.write('Città: errore nell\'apertura del ' +
                'file {0}.\n'.format(self.cities_file))
            sys.stderr.flush()

    def regions(self):
        return self.regions

    def regions_db(self):
        return self.regions.values()

    def provinces(self):
        return self.provinces

    def provinces_db(self):
        prov_w_reg = []
        for prov in self.provinces:
            temp = {}
            temp['nome'] = prov['nome']
            temp['regione'] = self.regions[int(prov['regione'])]
            prov_w_reg.append(temp)
        return prov_w_reg

    def cities(self):
        return self.cities

    def cities_db(self):
        cities_w_prov = []
        for city in self.cities:
            temp = {}
            temp['nome'] = city['nome']
            temp['cap'] = city['cap']
            # find the index of the province in self.provinces
            index = next(index for (index, d) in enumerate(self.provinces)
                if d['id'] == int(city['provincia']))
            temp['provincia'] = self.provinces[index]['nome']
            cities_w_prov.append(temp)
        return cities_w_prov

if __name__ == '__main__':
    parser = Parser(
        'reg_prov.csv',
        'cities.csv',
        'zips.csv'
    )
    regions = parser.regions_db()
    provinces = parser.provinces_db()
    cities = parser.cities_db()

    try:
        with open(path.join('..', 'regioni.pck'), 'w') as f:
            pickle.dump(regions, f)
        with open(path.join('..', 'province.pck'), 'w') as f:
            pickle.dump(provinces, f)
        with open(path.join('..', 'citta.pck'), 'w') as f:
            pickle.dump(cities, f)
    except IOError, pickle.PicklingError:
        sys.stderr.write('Problemi nel salvataggio dei file.')
        sys.stderr.flush()