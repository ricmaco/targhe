CREATE TABLE IF NOT EXISTS Regione(
	r_nome		TEXT PRIMARY KEY
);
CREATE TABLE IF NOT EXISTS Provincia(
	pr_nome		TEXT PRIMARY KEY,
	regione		TEXT NOT NULL,
	FOREIGN KEY(regione) REFERENCES Regione(r_nome)
);
CREATE TABLE IF NOT EXISTS Citta(
	c_id		INTEGER PRIMARY KEY,
	c_nome		TEXT,
	CAP			TEXT,
	provincia	TEXT NOT NULL,
	FOREIGN KEY(provincia) REFERENCES Provincia(pr_nome)
);
CREATE TABLE IF NOT EXISTS Persona(
	p_id		INTEGER PRIMARY KEY,
	p_nome 		TEXT,
	cognome		TEXT,
	data_nasc 	TEXT,
	luogo_nasc	INTEGER NOT NULL,
	via_res		TEXT,
	luogo_res	INTEGER NOT NULL,
	cat_doc 	TEXT,
	num_doc 	TEXT UNIQUE,
	FOREIGN KEY(luogo_nasc) REFERENCES Citta(c_id),
	FOREIGN KEY(luogo_res) REFERENCES Citta(c_id)
);
CREATE TABLE IF NOT EXISTS Veicolo(
	targa 			TEXT PRIMARY KEY,
	marca 			TEXT,
	modello 		TEXT,
	colore 			TEXT,
	tipo_traino 	TEXT,
	targa_traino 	TEXT UNIQUE,
	proprietario 	INTEGER NOT NULL,
	FOREIGN KEY(proprietario) REFERENCES Persona(p_id)
);
CREATE TABLE IF NOT EXISTS Stop(
	s_id 			INTEGER PRIMARY KEY,
	luogo 			INTEGER NOT NULL,
	data 			TEXT,
	norma_cds 		TEXT,
	contravvenzione INTEGER,
	veicolo 		TEXT NOT NULL,
	FOREIGN KEY(luogo) REFERENCES Citta(c_id),
	FOREIGN KEY(veicolo) REFERENCES Veicolo(targa)
);