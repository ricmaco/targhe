# -*- coding: utf-8 -*-

import os
import locale
from os import path

from config import parser
from lib import logger, utils
from lib.database import model, controller, insert as db_ins
from lib.ui import window, content, insert


class Main():

    ROOT = path.dirname(__file__)

    def __init__(self):
        # Intialize config parser
        conf = parser.Parser(self.ROOT)
        # set locale if available on windows or posix
        try:
            if os.name == 'nt':
                locale.setlocale(locale.LC_ALL, conf.get('lang_nt'))
            elif os.name == 'posix':
                locale.setlocale(locale.LC_ALL, conf.get('lang_posix'))
        except locale.Error:
            pass
        # check if data dir is present
        datadir = path.join(self.ROOT, conf.get('datadir'))
        utils.check_dir(datadir)
        log = logger.Logger(conf)
        db = model.Model(conf, log)
        ctrl = controller.Controller(log, db)
        app = window.Window(conf, db)
        cont = content.Content(app)
        db_insert = db_ins.Insert(ctrl)
        insert.Insert(cont, cont.insert_tab(), db_insert)
        app.mainloop()
        db.close()
        log.close()


if __name__ == '__main__':
    main = Main()
